package fragments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.daskvniti_gamocda.R
import com.google.firebase.auth.FirebaseAuth

class RegistrationFragment : Fragment(R.layout.registration_fragment) {

    private lateinit var email: EditText
    private lateinit var name: EditText
    private lateinit var lastname: EditText
    private lateinit var passwordOne: EditText
    private lateinit var passwordTwo: EditText
    private lateinit var checkBox: CheckBox
    private lateinit var registerBtn: Button
    private lateinit var exitBtn: Button
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        email = view?.findViewById(R.id.email)!!
        name = requireView().findViewById(R.id.name)
        lastname = requireView().findViewById(R.id.lastname)
        passwordOne = requireView().findViewById(R.id.password_one)
        passwordTwo = requireView().findViewById(R.id.password_two)
        checkBox = requireView().findViewById(R.id.checkBox)
        registerBtn = requireView().findViewById(R.id.register_button)
        exitBtn = requireView().findViewById(R.id.exit)

        registerBtn.setOnClickListener {
            if (validateEmail() && validatePasswordOne() && validatePasswordTwo() && validateCheckBox() && validateName() && validateLastname()) {
                val email = email.text.toString()
                val password = passwordOne.text.toString()

                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(activity, "registered successfully", Toast.LENGTH_SHORT)
                                .show()
                            startActivity(Intent(activity, HomeFragment::class.java))
                            //val action = RegisterFragmentDirections.RegisterFragmentto
                            // Navigation.findNavController(R.id.nav_auithzfdsf).navigate(action)
                            // navigate shecvla
                            activity?.finish()
                        } else {
                            Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show()
                        }
                    }
            }
        }

    }

    private fun validateEmail(): Boolean {
        if (email.text.isEmpty()) {
            Toast.makeText(activity, "mail bar is empty", Toast.LENGTH_SHORT).show()
            return false
        }
        if (!email.text.contains("@")) {
            Toast.makeText(activity, "mail doesn't contains @ ", Toast.LENGTH_SHORT).show()
            return false
        }
        if (email.text.last() == '.') {
            Toast.makeText(activity, "mail is ending with ' . '", Toast.LENGTH_SHORT).show()
            return false
        }
        val email = email.text.toString().split("@")
        if (email.size > 2) {
            Toast.makeText(activity, "mail contains more then one '@'", Toast.LENGTH_SHORT).show()
            return false

        }
        if (email[0].length < 5) {
            Toast.makeText(
                activity,
                "mail(first part) contains less then 6 letter",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }
        if (email[1].length < 6) {
            Toast.makeText(
                activity,
                "mail(second part) contains less then 6 letter",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }
        if (!email[1].contains(".")) {
            Toast.makeText(
                activity,
                "mail(second part) doesn't contains '.' ",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }

        return true
    }

    private fun validatePasswordOne(): Boolean {
        if (passwordOne.text.isEmpty()) {
            Toast.makeText(activity, "password bar is empty", Toast.LENGTH_SHORT).show()
            return false
        }

        if (passwordOne.text.length < 9) {
            Toast.makeText(activity, "password < 9", Toast.LENGTH_SHORT).show()
            return false
        }
        var digit: Boolean = false
        var letter: Boolean = false
        for (element in passwordOne.text) {
            val char = element
            if (char.isDigit()) {
                digit = true
            }
            if (char.isLetter()) {
                letter = true
            }
            if (digit && letter) {
                return true
            }
        }
        Toast.makeText(activity, "missing digits or letters", Toast.LENGTH_SHORT).show()
        return false
    }

    private fun validatePasswordTwo(): Boolean {
        if (passwordOne.text.toString() != passwordTwo.text.toString()) {
            Toast.makeText(activity, "Passwords doesn't match!", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    private fun validateCheckBox(): Boolean {
        if (checkBox.isChecked) {
            return true
        }
        return false
    }

    private fun validateName(): Boolean {
        if (name.text.isEmpty()) {
            return false
        }
        if (name.text.length < 2) {
            return false
        }
        for (i in name.text) {
            if (!i.isLetter()) {
                return false
            }
        }
        return true
    }

    private fun validateLastname(): Boolean {
        if (lastname.text.isEmpty()) {
            return false
        }
        if (lastname.text.length < 2) {
            return false
        }
        for (i in lastname.text) {
            if (!i.isLetter()) {
                return false
            }
        }
        return true
    }
}