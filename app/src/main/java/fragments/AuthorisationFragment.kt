package fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.example.daskvniti_gamocda.R

class AuthorisationFragment:Fragment(R.layout.autorisation_fragment) {
    private lateinit var email:EditText
    private lateinit var password:EditText
    private lateinit var loginBtn:Button
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        email = view.findViewById(R.id.email)
        password = view.findViewById(R.id.password)
        loginBtn = view.findViewById(R.id.login_button)
    }
}